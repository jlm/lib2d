/*
 * Copyright (C) 2019 Joseph Marshall
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

/**
 * @file lib2d.h
 * @author Joseph Marshall
 * @brief lib2d is a fast and simple 2D sprite rendering library for games licensed under the MIT.
 */

#ifndef __lib2d__
#define __lib2d__
#ifndef LIB2D_EXPORT
#if defined _WIN32
	#define LIB2D_EXPORT __declspec(dllexport)
#else
	#define LIB2D_EXPORT __attribute__((visibility("default")))
#endif
#endif
#include <stdint.h>
#include <stdbool.h>

#define LIB2D_VERSION_MAJOR 0
#define LIB2D_VERSION_MINOR 2
#define LIB2D_VERSION_PATCH 0

/**
 * @brief Passed to `lib2d_init()` to specify which graphics back-end to use.
 */
enum lib2d_render_backend {
    LIB2D_RENDER_BACKEND_GL=0
};


/**
 * Returns 0 on success and a negative number on failure. Call
 * `lib2d_get_error()` for more information if this call fails.
 *
 * Depending on the rendering backend used you may need to pass in relevant
 * context information. This is platform specific.
 * For OpenGL make sure the context is currently bound when you call this
 * function and pass NULL to render_context.
 */
LIB2D_EXPORT
int lib2d_init(enum lib2d_render_backend render_backend, void* render_context);

/**
 * Free resources created in `lib2d_init()`. All other lib2d resources
 * previously created, such as images, should be deleted before calling this
 * function.
 */
LIB2D_EXPORT
void lib2d_shutdown(void);

/**
 * Clears screen with the given color.
 */
LIB2D_EXPORT
void lib2d_clear(float r, float g, float b, float a);

/**
 * Call this with the size of your render target (e.g. your game's window.)
 */
LIB2D_EXPORT
void lib2d_viewport(int width, int height);

/**
 * Executes all draw commands issued with `lib2d_draw()` since the last call to this function.
 */
LIB2D_EXPORT
void lib2d_render(void);

/**
 * @brief Information on a loaded `lib2d_image`.
 */
typedef struct lib2d_image_info {
    /** Width in pixels of this image. */
    int w;

    /** Height in pixels of this image. */
    int h;
} lib2d_image_info;

/**
 * @brief References a bitmap that can be rendered by a `lib2d_drawable`.
 */
typedef struct lib2d_image lib2d_image;

/** 
 * @relates lib2d_image
 *
 * Load a bitmap image from the filesystem.
 *
 * Examples
 * ========
 *
 * ```
 * lib2d_image_info info;
 * lib2d_image* im = lib2d_image_load("bitmap.png", &info);
 * if (!im) {
 *     fprintf(stderr, "%s\n", lib2d_get_error());
 * }
 * lib2d_drawable d = {.w=info.w, .h=info.h, .image=im, .color=0xffffffff};
 * ```
 */
LIB2D_EXPORT
lib2d_image* lib2d_image_load(const char* path, lib2d_image_info* info);

LIB2D_EXPORT
lib2d_image* lib2d_image_load_buffer(unsigned char* data, int w, int h, int bpp);

LIB2D_EXPORT
lib2d_image* lib2d_image_load_png(unsigned char* data, int len, lib2d_image_info* info);

LIB2D_EXPORT
lib2d_image* lib2d_image_sub(lib2d_image* source, float l, float t, float r, float b);

/** 
 * @relates lib2d_image
 *
 * Deletes resources associated with an image. Do not use an image after it's
 * been deleted.
 */
LIB2D_EXPORT
void lib2d_image_delete(lib2d_image* image);


typedef struct lib2d_shader lib2d_shader;

LIB2D_EXPORT
lib2d_shader* lib2d_shader_new(const char* frag);


/** 
 * @brief An entity that renders a graphical source, such as an image, to the display.
 *
 * Examples
 * ========
 *
 * ```
 * // Render a white 100x100 square.
 * lib2d_drawable d = {.x=200, .y=200, .w=100, .h=100, .color=0xffffffff};
 * lib2d_draw(&d);
 * lib2d_render();
 * ```
 */
typedef struct lib2d_drawable {
    /** How many pixels from the left of the screen this drawable should render */
    float x;

    /** How many pixels down from the top of the screen this drawable should render */
    float y;

    /** Width of this drawable  */
    float w;

    /** Height of this drawable  */
    float h;

    /** Rotation in radians */
    float rot;

    /** What image, if any, should be drawn. If NULL, a solid color will be drawn. */
    lib2d_image* image;

    /**
     * Multiplies each pixel value of the referenced image by this color,
     * stored in an RGBA format with the alpha channel being the least
     * significant byte.
     *
     * Use a value of `0xffffffff` to render the original image.
     */
    uint32_t color;
} lib2d_drawable;


typedef struct lib2d_render_mode {
    lib2d_shader* shader;
    bool blend;
    bool front_to_back;
} lib2d_render_mode;


/** 
 * @relates lib2d_drawable
 *
 * Schedule a `lib2d_drawable` to be drawn when `lib2d_render()` is called.
 */
LIB2D_EXPORT
void lib2d_draw(const lib2d_drawable* drawable);


/**
 * @brief A font that can be used to render UTF-8 text using `lib2d_text()`.
 *
 * Examples
 * ========
 *
 * ```
 * lib2d_font font;
 * lib2d_font_load("FreeSans.ttf", &font);
 * lib2d_text(font, 80, "Hello Lib2D!", 200, 300, 0x000000ff);
 * ```
 */
typedef struct lib2d_font {
    /** @private */
    void* internal;
} lib2d_font;

/** 
 * @relates lib2d_font
 *
 * Load a TrueType font from the filesystem. Returns 0 on success.
 *
 * Examples
 * ========
 *
 * ```
 * lib2d_font font;
 * if (lib2d_font_load("FreeSans.ttf", &font) != 0) {
 *     fprintf(stderr, "%s\n", lib2d_get_error());
 * }
 * ```
 */
LIB2D_EXPORT
int lib2d_font_load(const char* path, lib2d_font* font);

/** 
 * @relates lib2d_font
 *
 * Frees all resources allocated by and for a `lib2d_font`.
 */
LIB2D_EXPORT
void lib2d_font_delete(lib2d_font font);

/** 
 * @relates lib2d_font
 *
 * Schedules UTF-8 text to be rendered to the screen when `lib2d_render()` is called.
 */
LIB2D_EXPORT
void lib2d_text(const lib2d_font font, float font_size,
        const char* text, int x, int y, uint32_t color);



/**
 * Retrieves the error message for the last failure.
 *
 * Examples
 * ========
 *
 * ```
 * if (lib2d_init(LIB2D_RENDER_BACKEND_GL, 0) != 0) {
 *     fprintf(stderr, "%s\n", lib2d_get_error());
 * }
 * ```
 */
LIB2D_EXPORT
const char* lib2d_get_error(void);

#endif
