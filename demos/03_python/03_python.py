""""Copyright (C) 2019  Joseph Marshall

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
"""

def main():
    pygame.display.init()
    screen = pygame.display.set_mode((800, 600), HWSURFACE|OPENGL|DOUBLEBUF)
    print("lib2d {}".format(lib2d.__version__))
    lib2d.init()
    lib2d.viewport(800, 600)

    lib2d_root = os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))

    d = lib2d.Drawable(
            os.path.join(lib2d_root, "www", "theme", "img", "logo.png"),
            x=200, y=200)

    font = lib2d.Font(os.path.join(lib2d_root, "demos", "04_text", "FreeSans.ttf"))

    glClearColor(1.0, 1.0, 1.0, 0.0)
    running = True
    while running:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                running = False
                break

        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        font.text("Hello Lib2D!", font_size=40, x=200, y=150)
        d.draw()
        lib2d.render()
        pygame.display.flip()

    lib2d.shutdown()



import sys, os

try:
    import pygame
    from pygame.locals import *
except ImportError:
    print("This example requires pygame to be installed")
    sys.exit()

try:
    import lib2d
except ImportError:
    print("lib2d not installed, run:")
    print("$ sudo {} -m pip install lib2d".format(sys.executable))
    sys.exit()

try:
    from OpenGL.GL import *
    from OpenGL.GLU import *
except ImportError:
    print("Missing PyOpenGL, run:")
    print("$ sudo {} -m pip install PyOpenGL".format(sys.executable))
    sys.exit()


if __name__ == "__main__":
    main()
